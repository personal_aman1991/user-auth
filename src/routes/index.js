import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from '../container/App';
import LoginComponent from '../components/login';
import RegisterComponent from '../components/register';
import DashboardComponent from '../components/dashboard';

function isLoggedIn() {
  if (localStorage.getItem('token')) {
    return true;
  }

  return false;
}

function requireAuth(nextState, replace) {
  if (!isLoggedIn()) {
    replace({
      pathname: '/login'
    });
  }
}

export default (
  <Route path='/' component={App}>
    <IndexRoute component={LoginComponent} />
    <Route path='login' component={LoginComponent} />
    <Route path='register' component={RegisterComponent} />
    <Route path='dashboard' component={DashboardComponent} onEnter={requireAuth} />
  </Route>
);
