import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

class DashboardComponent extends Component {

  render() {

    return (
      <div>
        <h3>Welcome to the Dashboard.</h3>

        Want to Logout? <Link to='login'>Logout</Link>
      </div>
    );
  }
}
const mapStateToProps = (response) => ({response});
export default connect(mapStateToProps)(DashboardComponent);
